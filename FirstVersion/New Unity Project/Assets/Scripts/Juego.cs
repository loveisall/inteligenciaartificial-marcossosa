﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Juego : MonoBehaviour {

	[SerializeField]
	private int SizeX = 10;
	[SerializeField]
	private int SizeZ = 10;

	private GameObject[,] _tiles;
	private int[,] GridValues;

	private List<Nodo> _camino = new List<Nodo>();
	private Nodo _inicio;
	private Nodo _meta;

	private bool _loading = true;
	private bool _canDestroy = false;

	private DepthFirstSearch depth= new DepthFirstSearch();
	private BreadthFirstSearch breadth = new BreadthFirstSearch();
	private AEstrella astar = new AEstrella();


	void Start () {		
		//Vaciado de la grilla
		GridValues= new int[SizeX,SizeZ];
		for (int i = 0; i < SizeX; i++) {
			for (int j = 0; j < SizeZ; j++) {
				GridValues [i, j] = 0;
			}
		}
	
		//Armado manual de los valores de la grilla 0 es nodo , 1 es start, 2 es meta y 3 es obstaculo.
		GridValues[5, 1] = 1;
		GridValues[9, 9] = 2;
		GridValues[3, 1] = 3;
		GridValues[3, 2] = 3;
		GridValues[3, 3] = 3;
		GridValues[3, 4] = 3;
		GridValues[7, 7] = 3;
		GridValues[4, 6] = 3;
		GridValues[5, 5] = 3;
		GridValues[2, 7] = 3;
		GridValues[4, 0] = 3;
		GridValues[4, 1] = 3;
		LoadMap();
	}

	public void LoadMap() {
		_loading = true;

		if(_canDestroy) {
		//Destruyo la grilla
			for(int x = 0; x < SizeX; x++) {
				for(int z = 0; z < SizeZ; z++) {
					GameObject tile = _tiles[x,z];
					Destroy(tile);
				}
			}
		}

		//Genero la grilla
		_tiles = new GameObject[SizeX,SizeZ];
		for(int x = 0; x < SizeX; x++) {
			for(int z = 0; z < SizeZ; z++) {
				GameObject tile = Instantiate(Resources.Load("Prefab/tile")) as GameObject;
				tile.name = "Tile_"+x+"_"+z;
				tile.transform.position = new Vector3( (float) x, 0, (float) z);

				Nodo n = tile.GetComponent("Nodo") as Nodo;
				n.Visited = false;
				n.G = 0;
				n.H = 0;
				n.F = 0;
				n.Path = false;

				_tiles[x,z] = tile;
			}
		}

		//Asociando la grilla.
		for(int x = 0; x < SizeX; x++) {
			for(int z = 0; z < SizeZ; z++) {				
				Nodo n = _tiles[x,z].GetComponent("Nodo") as Nodo;
				if( (x - 1) >= 0 && (z + 1) < SizeZ ) n.adjacent.Add( _tiles[x-1,z+1].GetComponent("Nodo") as Nodo);	//1
				if( (z + 1) < SizeZ ) n.adjacent.Add( _tiles[x,z+1].GetComponent("Nodo") as Nodo);						//2
				if( (x + 1) < SizeX && (z + 1) < SizeZ ) n.adjacent.Add( _tiles[x+1,z+1].GetComponent("Nodo") as Nodo);	//3
				if( (x - 1) >= 0 ) n.adjacent.Add( _tiles[x-1,z].GetComponent("Nodo") as Nodo);						//4
				if( (x + 1) < SizeX) n.adjacent.Add( _tiles[x+1,z].GetComponent("Nodo") as Nodo);						//5
				if( (x - 1) >= 0 && (z - 1) >= 0 ) n.adjacent.Add( _tiles[x-1,z-1].GetComponent("Nodo") as Nodo);	//6
				if( (z - 1) >= 0 ) n.adjacent.Add( _tiles[x,z-1].GetComponent("Nodo") as Nodo);						//7
				if( (x + 1) < SizeX && (z - 1) >= 0 ) n.adjacent.Add( _tiles[x+1,z-1].GetComponent("Nodo") as Nodo);	//8
			}					
		}

		for (int x = 0; x < SizeX; x++) {
			for (int z = 0; z < SizeZ; z++) {
				Nodo _obs = null;
				switch (GridValues [x, z]) {
				case 0://Nada
					
					break;
				case 1://Inicio
					_inicio = _tiles [x, z].GetComponent("Nodo") as Nodo;
					_inicio.Status = Nodo.INICIO;
					break;
				case 2://Meta
					_meta = _tiles [x, z].GetComponent("Nodo") as Nodo;
					_meta.Status = Nodo.FINAL;
					break;
				case 3:	//obstaculo
					_obs = _tiles [x, z].GetComponent("Nodo") as Nodo;
					_obs.Status = Nodo.OBSTACULO;
					break;
				default:
					break;	

				}
			}
		
		}

	//	Camera.main.transform.position = new Vector3( (float) (SizeX / 2), Camera.main.gameObject.transform.position.y,(float) -(SizeZ / 10f));		
		_loading = false;
		_canDestroy = true;
	}

	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown (KeyCode.Alpha1)) {
			ResetMapa();
			InciarBusqueda (0);		
		}

		if (Input.GetKeyDown (KeyCode.Alpha2)) {
			ResetMapa ();
			InciarBusqueda (1);		

		}

		if (Input.GetKeyDown (KeyCode.Alpha3)) {
			ResetMapa ();
			InciarBusqueda (2);
		}

		if(!_loading) {
			for(int x = 0; x < SizeX; x++) {
				for(int z = 0; z < SizeZ; z++) {
					Nodo n = _tiles[x,z].GetComponent("Nodo") as Nodo;

					switch (n.Status) {
					case Nodo.LIMPIO:
						if(n.Path) _tiles[x,z].GetComponent<Renderer>().material.color = Color.cyan;
						else if(n.Visited) _tiles[x,z].GetComponent<Renderer>().material.color = Color.Lerp(Color.white, Color.black, 0.5f);
						else _tiles[x,z].GetComponent<Renderer>().material.color = Color.Lerp(Color.white, Color.black, 0.6f);
						break;
					case Nodo.INICIO:
						_tiles[x,z].GetComponent<Renderer>().material.color = Color.blue;
						break;	
					case Nodo.FINAL:
						_tiles[x,z].GetComponent<Renderer>().material.color = Color.yellow;
						break;
					case Nodo.OBSTACULO:
						_tiles[x,z].GetComponent<Renderer>().material.color = Color.red;
						break;		
					}

				}
			}
		}
	}

	public void Find(int _method) {	
		bool _found = false;

		switch (_method) {
		case 0:	//DFS
			_found = depth.Find (_inicio);
			_camino = depth.GetPath ();
			break;
		case 1:	//BFS
			_found = breadth.Find (_inicio);
			_camino = breadth.GetPath ();
			break;
		case 2:	//A*
			_found = astar.Find(_inicio, _meta);
			_camino = astar.GetPath ();
			break;
		}
			
		//Search for end-node.
		if (_found) {	
			if(Debug.isDebugBuild) {
				string _out = "";

				foreach (Nodo n in _camino) {
					_out += n.name + "hola ";
				}
				Debug.Log (_out);
			}


		} else {
			if(Debug.isDebugBuild) {
				Debug.Log("Search: Path Could not be found!");
			}
		}
	}

	//Triggers the Search
	public void InciarBusqueda(int method) {
		Find (method);
	}

	//Resets the grid to use the same map with a different search algorithm.
	public void ResetMapa() {
		_camino = new List<Nodo>();

		depth = new DepthFirstSearch();
		breadth = new BreadthFirstSearch();
		astar = new AEstrella();

		Debug.Log("Reseting Grid:");
		for(int x = 0; x < SizeX; x++) {
			for(int z = 0; z < SizeZ; z++) {
				Nodo n = _tiles[x,z].GetComponent("Nodo") as Nodo;
				n.Visited = false;
				n.parent = null;
				n.G = 0;
				n.H = 0;
				n.F = 0;
				n.Path = false;
			}
		}
	}
}
