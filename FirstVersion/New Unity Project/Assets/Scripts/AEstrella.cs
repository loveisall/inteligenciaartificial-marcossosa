﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AEstrella {
	private List<Nodo> _path = new List<Nodo>();
	private List<Nodo> _order = new List<Nodo>();

	//private Queue<Nodo> _open = new Queue<Nodo>();
	private List<Nodo> _open = new List<Nodo>();
	private Queue<Nodo> _close = new Queue<Nodo>();

	private int Distance(Nodo _start, Nodo _target) {
		int x = (int) Mathf.Abs( (float) (_start.X - _target.X));
		int z = (int) Mathf.Abs( (float) (_start.Z - _target.Z));
		int s = (x < z) ? x : z;
		return (s * 14) + ( (int) Mathf.Abs( (float) (x - z)) * 10);
	}

	public bool Find(Nodo _start, Nodo _target) {
		_start.Visited = true;
		_start.H = Distance (_start, _target);	
		_start.G = 0;
		_open.Add(_start);

		while(_open.Count > 0) {
			Nodo c = _open[0];
			_open.RemoveAt(0);
			c.Visited = true;

			//End Nodo Found
			if(c.Status == Nodo.FINAL ) {		
				//Backtrack using Parent for path.
				while(c != null) {
					_path.Add(c);
					c.Path = true;
					c = c.parent;
				}

				_path.Reverse();	//Reverse the Path from END->START to START->END 				
				return true;
			} 
			_close.Enqueue(c);

			for (int i = 0; i < c.adjacent.Count; i++) {
				Nodo n = c.adjacent[i];

				if (n.Status != Nodo.OBSTACULO) {		//If the node is NOT a wall or Null
					int h = Distance(n, _target);
					int g = n.G;

					switch(i) {
					case 1:case 3:case 6:case 8:
						g += 14;
						break;
					case 2:case 4:case 5:case 7:
						g += 10;
						break;
					}

					if(_close.Contains(n)) continue;

					if(_open.Contains(c) ) {
						if(g < n.G ) {
							n.parent = c;
							n.H = h;
							n.G = g;
							n.F = n.G + n.H;
						}
					} else {
						n.parent = c;
						n.H = h;
						n.G = g;
						n.F = n.G + n.H;
						_open.Add(n);
					}

				}
			}

			_open.Sort();
		}

		return false;
	}

	public List<Nodo> GetPath() { return _path; }
	public List<Nodo> GetOrder() { return _order; }
}