﻿
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DepthFirstSearch {
	private List<Nodo> _camino = new List<Nodo>();
	private List<Nodo> _orden = new List<Nodo>();

	public bool Find(Nodo _start) {
		_start.Visited = true;
		_orden.Add (_start);

		if (_start.Status == Nodo.FINAL) {
			_camino.Add (_start);
			_start.Path = true;
			return true;
		} else {
			for (int i = 0; i < _start.adjacent.Count; i++) {
				if (_start.adjacent [i].IsValid ()) {
					if (Find (_start.adjacent [i])) {
						_camino.Insert (0, _start);
						_start.Path = true;
						return true;
					}
				}
			}
		}

		return false;
	}

	public List<Nodo> GetPath() { return _camino; }
	public List<Nodo> GetOrder() { return _orden; }
}