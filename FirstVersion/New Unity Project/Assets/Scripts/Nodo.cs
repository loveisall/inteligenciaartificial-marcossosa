﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class Nodo : MonoBehaviour, IComparable<Nodo> {
	public const int OBSTACULO = 1;
	public const int INICIO = 2;
	public const int FINAL = 3;
	public const int LIMPIO = 0;
	public const int CAMINO = 4;

	public int G = 0;
	public int H = 0;
	public int F = 0;

	public bool Visited = false;
	public bool Path = false;
	public List<Nodo> adjacent = null; 	//Used for ALL searches.
	public Nodo parent = null;			//Used for BFS.

	private int _status = 0;

	public int Status {
		get { return _status; }
		set { if(value >= 0 && value < 5) _status = value; }
	} 

	public int X {
		get { return (int) gameObject.transform.position.x; }
	}

	public int Z {
		get { return (int) gameObject.transform.position.z; }
	}

	/**
	 *	Game Specific Validation
	 */	
	public bool IsValid() {
		return !Visited && (_status != Nodo.OBSTACULO); 
	}

	/**
	 *	Compare this F to another F
	 */
	public int CompareTo(Nodo n) {
		if(n == null) return 1;

		return this.H.CompareTo(n.H);
	}


}