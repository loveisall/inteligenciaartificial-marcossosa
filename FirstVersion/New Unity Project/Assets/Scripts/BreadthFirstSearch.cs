﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BreadthFirstSearch {
	private List<Nodo> _camino = new List<Nodo>();
	private List<Nodo> _orden = new List<Nodo>();
	private Queue<Nodo> _cola = new Queue<Nodo>();

	public bool Find(Nodo _start) {
		_start.Visited = true;
		_orden.Add (_start);
		_cola.Enqueue(_start);

		while(_cola.Count > 0) {
			Nodo n = _cola.Dequeue();

			if (n.Status == Nodo.FINAL) {
				Nodo t = n;
				while(t != null) {
					_camino.Add(t);
					t.Path = true;
					t = t.parent;
				}
				_camino.Reverse();
				return true;
			}
			for (int i = 0; i < n.adjacent.Count; i++) {
				if (n.adjacent [i].IsValid ()) {
					n.adjacent[i].parent = n;
					n.adjacent[i].Visited = true;
					_orden.Add (n.adjacent[i]);
					_cola.Enqueue(n.adjacent[i]);
				}
			}

		}

		return false;
	}

	public List<Nodo> GetPath() { return _camino; }
	public List<Nodo> GetOrder() { return _orden; }
}